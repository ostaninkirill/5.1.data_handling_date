package src.Demo;

import src.Service.DateService;
import src.Service.IDateService;

import java.time.LocalDateTime;

public class DemoService implements IDemoService {

    @Override
    public void execute() {
        DateService dateService = new DateService();

        // Задача 1.1
        System.out.println("Задача 1.1");
        System.out.println("С момента рождения прошло:  ");
        dateService.yourAge(LocalDateTime.of(1998,9,3,0,0,0));

        System.out.println();

        // Задача 1.2
        System.out.println("Задача 1.2");
        String firstDate = "03.12.2005";
        String secondDate = "10.12.2012";
        System.out.println("Разница между датой " + firstDate + " и " + secondDate + " - " +  dateService.difference(firstDate, secondDate) + " дней");

        System.out.println();

        // Задача 1.3
        System.out.println("Задача 1.3");
        String dateStr1 = "Friday, Aug 10, 2018 12:10:56 PM";
        System.out.print("Конвертирование даты " + dateStr1 + " в вид ");
        dateService.converterDate(dateStr1);

        System.out.println();

        // Задача 1.3 под звездочкой
        System.out.println("Задача 1.3 под звездочкой");
        String dateStr2 = "2016-08-16T10:15:30+08:00";
        System.out.println("Конвертирование даты " + dateStr2 + " в дату со временем с учетом часового пояса Ижевска" );
        System.out.println("Результат: " + dateService.converterToIzhevsk(dateStr2));



    }
}
