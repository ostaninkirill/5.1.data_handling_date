package src.Service;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Locale;

public class DateService implements IDateService {

    // Задача 1.1
    @Override
    public void yourAge (LocalDateTime birthday) {

        LocalDateTime today = LocalDateTime.now();
        System.out.println(ChronoUnit.SECONDS.between(birthday, today) + " секунд ");
        System.out.println(ChronoUnit.MINUTES.between(birthday, today) + " минут ");
        System.out.println(ChronoUnit.HOURS.between(birthday, today) + " часов ");
        System.out.println(ChronoUnit.DAYS.between(birthday, today) + " дней ");
        System.out.println(ChronoUnit.MONTHS.between(birthday, today) + " месяцев ");
        System.out.println(ChronoUnit.YEARS.between(birthday, today) + " лет");
    }

    // Задача 1.2
    @Override
    public long difference (String firstIncomingDate, String secondIncomingDate) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");

        LocalDate firstDate = LocalDate.parse(firstIncomingDate, formatter);
        LocalDate secondDate = LocalDate.parse(secondIncomingDate, formatter);
        long days = ChronoUnit.DAYS.between(firstDate, secondDate);
        if (days >= 0){
            return days;
        }
        else {
            return ChronoUnit.DAYS.between(secondDate, firstDate);
        }
    }

    // Задача 1.3
    @Override
    public void converterDate (String date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("EEEE, MMM d, yyyy HH:mm:ss a", Locale.US);
        LocalDate localDate = LocalDate.parse(date, formatter);
        System.out.println(localDate);
    }


    // Задача 1.3 под звездочкой
    @Override
    public String converterToIzhevsk (String datePoyas) {
        ZonedDateTime dateTime = ZonedDateTime.parse(datePoyas).withZoneSameInstant(ZoneId.of("Europe/Samara"));
        DateTimeFormatter formatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME;
        return dateTime.format(formatter);
    }
}
