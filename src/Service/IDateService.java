package src.Service;

import java.time.LocalDateTime;

public interface IDateService {
    void yourAge (LocalDateTime birthday);
    long difference (String firstIncomingDate, String secondIncomingDate);
    void converterDate (String date);
    String converterToIzhevsk (String datePoyas);
}
